window.addEventListener('DOMContentLoaded', async () => {                       // Don't forget the async

    const url = 'http://localhost:8000/api/locations/';

    try {
    const response = await fetch(url);
    // console.log('response', response)

    if (response.ok) {
        const data = await response.json();
        // console.log('data works', data)

        const selectTag = document.getElementById('location')

        for (let location of data.locations) {
            const option = document.createElement('option')
            option.value = location.id
            option.innerHTML = location.name
            selectTag.appendChild(option)
        }

    } else {
        throw new Error('Response is not OK')
    }
    } catch (error) {
        console.error("There has been an error")
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {                         // Don't forget the async
        event.preventDefault();
        // console.log('need to submit the form data');
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';             // The start of the code block that sends data to the server
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
              'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            // console.log(newConference);
        }                                                                       // The end of the code block that sends data to the server

    });

});
